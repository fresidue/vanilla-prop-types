// test/index.js
'use strict';

describe('prop-types', () => {

  it('testing prop-types has begun', () => 0);

  require('./constants.test');
  require('./config-input-validation.test');
  require('./is-required.test');

  require('./prop-types.test');

  require('./comprehensive-example.test');
  require('./nested-example.test');
  require('./validate-vs-check.test');
  require('./error-injector.test');
  require('./creating-custom-prop-types.test');

});
