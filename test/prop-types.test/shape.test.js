'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('shape options', () => {

  it('some valids', () => {
    const valids = [
      // trivial
      undefined,
      null,
      false,
      0,
      '',
      {},
      // isExact falsy
      {isExact: false},
      {isExact: null},
      {isExact: undefined},
      {isExact: 0},
      // isExact truthy
      {isExact: true},
      {isExact: 'any value really'},
      {isExact: new Error('custom error')},
      {isExact: () => 'any function will pass through the initialilzation stage'},
      // blacklist
      {blacklist: []},
      {blacklist: ['adsf']},
      {blacklist: ['adsf', 'adsff', 'adsfff', 'adsffff']},
      // blacklist-error
      {blacklistError: 'custom blacklist message'},
      {blacklistError: new Error('custom blacklist error')},
      {blacklistError: () => nil}, // any function passes first inspection
      // isExact & blacklist can co-exist
      {isExact: true, blacklist: ['asdf'], blacklistError: 'custom blacklist message'},
    ];
    _.each(valids, (validShapeOptions) => {
      assert.doesNotThrow(() => checkProps({
        shapeProp: PropTypes.shape({
          arp: PropTypes.any,
        }, validShapeOptions),
      }));
    });
  });

  it('some invalids', () => {
    const invalids = [
      {
        options: {anythingNotAllowed: true, two: 2},
        mess: /Invalid shape options \[anythingNotAllowed, two\]/,
      },
      {
        options: {isExact: 23},
        mess: /Invalid shape "isExact" option included. If truthy, it must be a boolean, a string \(which gets converted to an error\), an error object \(which simply gets returned\), or a function that returns or throws an error/,
      },
      {
        options: {blacklist: 23},
        mess: /Invalid shape option blacklist, must be an array/,
      },
      {
        options: {blacklist: [23]},
        mess: /Invalid shape option blacklist item, must be a string: 23/,
      },
      {
        options: {blacklistError: 23},
        mess: /Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}/,
      },
    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({
        arp: PropTypes.shape({}, invalid.options),
      }), invalid.mess);
    });
  });

  it('props that breaks shape isExact option', () => {
    const invalids = [
      {
        isExact: true,
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: PropTypes.shape with isExact=true has extra (unallowed) props: [blue]',
      },
      {
        isExact: true,
        props: {shapeProp: {blue: 1, red: 2, green: 3}},
        mess: 'PropTypes validation error at [shapeProp]: PropTypes.shape with isExact=true has extra (unallowed) props: [blue, red, green]',
      },
      {
        isExact: true,
        props: {shapeProp: {blue: 1, red: 2, green: 3, purple: 4}},
        mess: 'PropTypes validation error at [shapeProp]: PropTypes.shape with isExact=true has extra (unallowed) props: [blue, red, green, ...]',
      },
      // // iterate over different "isExact" forms
      {
        isExact: 'custom message',
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: custom message',
      },
      {
        isExact: new Error('custom error'),
        props: {shapeProp: {blue: 1}},
        mess: 'custom error',
      },
      // // valid functionals
      {
        isExact: () => true,
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: PropTypes.shape with isExact=true has extra (unallowed) props: [blue]',
      },
      {
        isExact: () => 'generated custom message',
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: generated custom message',
      },
      {
        isExact: () => new Error('generated custom error'),
        props: {shapeProp: {blue: 1}},
        mess: 'generated custom error',
      },
      // invalid functionals - still work but..
      {
        isExact: () => 23,
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: Invalid "isExact" option, which when truthy must be a boolean, string, an error object, or a function that throws an error, or returns a boolean, string, or an error. The error generator gets called with {prop, propnName, props, extraPropNames}). Extraneous props found with isExact=errorGenerator: [blue]',
      },
    ];
    _.each(invalids, invalid => {
      const check = checkProps({
        shapeProp: PropTypes.shape({}, {isExact: invalid.isExact}),
      });
      const mess = check(invalid.props);
      assert.strictEqual(mess, invalid.mess);
    });
  });

  it('props that breaks shape blacklist & blacklistError options', () => {
    const invalids = [
      // // no blacklistError
      {
        options: {blacklist: ['blue']},
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: Props from blacklist found, with keys: [blue]',
      },
      {
        options: {blacklist: ['blue', 'red', 'green']},
        props: {shapeProp: {blue: 1, red: 2, green: 3}},
        mess: 'PropTypes validation error at [shapeProp]: Props from blacklist found, with keys: [blue, red, green]',
      },
      {
        options: {blacklist: ['blue', 'red', 'green', 'purple']},
        props: {shapeProp: {blue: 1, red: 2, green: 3, purple: 4}},
        mess: 'PropTypes validation error at [shapeProp]: Props from blacklist found, with keys: [blue, red, green, ...]',
      },
      // // iterate over different "blacklistError" forms
      {
        options: {blacklist: ['blue'], blacklistError: 'custom message'},
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: custom message',
      },
      {
        options: {blacklist: ['blue'], blacklistError: new Error('custom error')},
        props: {shapeProp: {blue: 1}},
        mess: 'custom error',
      },
      // // valid functionals
      {
        options: {blacklist: ['blue'], blacklistError: () => 'generated custom message'},
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: generated custom message',
      },
      {
        options: {blacklist: ['blue'], blacklistError: () => new Error('generated custom error')},
        props: {shapeProp: {blue: 1}},
        mess: 'generated custom error',
      },
      {
        options: {blacklist: ['blue'], blacklistError: () => 23},
        props: {shapeProp: {blue: 1}},
        mess: 'PropTypes validation error at [shapeProp]: Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}). Props with blacklistedPropsFound found: [blue]',
      },
    ];
    _.each(invalids, invalid => {
      const check = checkProps({
        shapeProp: PropTypes.shape({}, invalid.options),
      });
      const mess = check(invalid.props);
      assert.strictEqual(mess, invalid.mess);
    });
  });

});

describe('shape (propType)', () => {

  it('some valid propTypes', () => {
    const valids = [
      {},
      [],
      () => {}, // is an object..
      {derp: PropTypes.any},
      {derp: PropTypes.string},
      {derp: PropTypes.number.isRequired},
      {derp: PropTypes.instanceOf(Date)},
      {derp: PropTypes.shape({smerp: PropTypes.any.isRequired})},
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({shapeProp: PropTypes.shape(valid)}));
    });
  });

  it('some invalid propTypes', () => {
    const derp = {};
    const invalids = [
      {shape: null, mess: /the "shape" input must be an object with PropTypes as props/},
      {shape: 23, mess: /the "shape" input must be an object with PropTypes as props/},
    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({shapeProp: PropTypes.shape(invalid.types)}), invalid.mess);
    });
  });

  it('shape - default', () => {
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    })});
    const checkees = [
      {props: {shapeProp: {}}, mess: null},
      {props: {shapeProp: {a: 'asdf', b: 23}}, mess: null},
      {props: {}, mess: null},
      {props: {shapeProp: undefined}, mess: null},
      {props: {shapeProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('shape - isRequired', () => {
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    }).isRequired});
    const checkees = [
      {props: {shapeProp: {}}, mess: null},
      {props: {shapeProp: {a: 'asdf', b: 23}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {shapeProp: undefined}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {shapeProp: null}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('shape - isRequiredOrNull', () => {
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    }).isRequiredOrNull});
    const checkees = [
      {props: {shapeProp: {}}, mess: null},
      {props: {shapeProp: {a: 'asdf', b: 23}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {shapeProp: undefined}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {shapeProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('shape.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    }).custom(customFn)});
    const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
    assert.strictEqual(mess1, null);
    const mess2 = check({shapeProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({shapeProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({shapeProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
  });

  it('shape.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    }).isRequired.custom(customFn)});
    const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
    assert.strictEqual(mess1, null);
    const mess2 = check({shapeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({shapeProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({shapeProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
  });

  it('shape.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    }).isRequiredOrNull.custom(customFn)});
    const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
    assert.strictEqual(mess1, null);
    const mess2 = check({shapeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({shapeProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({shapeProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
  });

  it('shape.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    }).custom(customFn).isRequired});
    const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
    assert.strictEqual(mess1, null);
    const mess2 = check({shapeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({shapeProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({shapeProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
  });

  it('shape.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({shapeProp: PropTypes.shape({
      derp: PropTypes.string,
      smerp: PropTypes.number,
    }).custom(customFn).isRequiredOrNull});
    const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
    assert.strictEqual(mess1, null);
    const mess2 = check({shapeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({shapeProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({shapeProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
  });

  it('shape - excess props (is not generally a failure)', () => {
    const check = checkProps({
      shapeProp: PropTypes.shape({
        blerp: PropTypes.string.isRequired,
      }),
    });
    const valids = [
      {shapeProp: undefined},
      {shapeProp: null},
      {shapeProp: {blerp: 'asdf'}},
      {shapeProp: {blerp: 'asdf', slerp: 'sksksk', derp: () => 0}},
    ];
    _.each(valids, valid => {
      const mess = check(valid);
      assert.strictEqual(mess, null);
    });
  });

});
