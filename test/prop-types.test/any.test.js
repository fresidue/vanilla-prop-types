'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('any', () => {

  it('any', () => {
    const check = checkProps({anyProp: PropTypes.any});
    const mess0 = check({anyProp: 'a string or anything should work'});
    assert.strictEqual(mess0, null);
    const mess2 = check({});
    assert.strictEqual(mess2, null);
    const mess3 = check({anyProp: undefined});
    assert.strictEqual(mess3, null);
    const mess4 = check({anyProp: null});
    assert.strictEqual(mess4, null);
  });

  it('any.isRequired', () => {
    const check = checkProps({anyProp: PropTypes.any.isRequired});
    const mess1 = check({anyProp: 'a string or anything should work'});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({anyProp: undefined});
    assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({anyProp: null});
    assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
  });

  it('any.isRequiredOrNull', () => {
    const check = checkProps({anyProp: PropTypes.any.isRequiredOrNull});
    const mess1 = check({anyProp: 'a string or anything should work'});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({anyProp: undefined});
    assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess4 = check({anyProp: null});
    assert.strictEqual(mess4, null);
  });

  it('any.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({anyProp: PropTypes.any.custom(customFn)});
    const mess1 = check({anyProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({anyProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({anyProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({anyProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
  });

  it('any.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({anyProp: PropTypes.any.isRequired.custom(customFn)});
    const mess1 = check({anyProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({anyProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({anyProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({anyProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
  });

  it('any.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({anyProp: PropTypes.any.isRequiredOrNull.custom(customFn)});
    const mess1 = check({anyProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({anyProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({anyProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({anyProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
  });

  it('any.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({anyProp: PropTypes.any.custom(customFn).isRequired});
    const mess1 = check({anyProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({anyProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({anyProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({anyProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
  });

  it('any.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({anyProp: PropTypes.any.custom(customFn).isRequiredOrNull});
    const mess1 = check({anyProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({anyProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({anyProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({anyProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
  });

});
