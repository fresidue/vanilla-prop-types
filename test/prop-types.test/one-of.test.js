'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('oneOf (enum)', () => {

  it('some valid propEnums', () => {
    const valids = [
      [],
      ['a'],
      ['slksdf', 23, {}],
      [true, false],
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({oneOfProp: PropTypes.oneOf(valid)}));
    });
  });

  it('some invalid propEnums', () => {
    const derp = {};
    const invalids = [
      {propEnum: 'notAnArray', mess: /propEnum must be an array/},
      {propEnum: [1, 1, 2], mess: /propEnum must contain unique values/},
      {propEnum: [derp, derp], mess: /propEnum must contain unique values/},
      {propEnum: ['one', null, 'two'], mess: /propEnum cannot contain undefined or null/},
      {propEnum: ['one', undefined, 'two'], mess: /propEnum cannot contain undefined or null/},
    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({oneOfProp: PropTypes.oneOf(invalid.propEnum)}), invalid.mess);
    });
  });

  it('oneOf - default', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum)});
    const checkees = [
      {props: {oneOfProp: 'asdf'}, mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp]'},
      {props: {oneOfProp: 'berp'}, mess: null},
      {props: {oneOfProp: derp}, mess: null},
      {props: {oneOfProp: String('berp')}, mess: null},
      {props: {}, mess: null},
      {props: {oneOfProp: undefined}, mess: null},
      {props: {oneOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('oneOf - when enum has more than 3 the enum list gets cropped', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp', 'snerp'];
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum)});
    const checkee = {
      props: {oneOfProp: 'asdf'},
      mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp, ...]',
    };
    const mess = check(checkee.props);
    assert.strictEqual(mess, checkee.mess);
  });

  it('oneOf.isRequired', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequired});
    const checkees = [
      {props: {oneOfProp: 'asdf'}, mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp]'},
      {props: {oneOfProp: 'berp'}, mess: null},
      {props: {oneOfProp: derp}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {oneOfProp: undefined}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {oneOfProp: null}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('oneOf.isRequiredOrNull', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequiredOrNull});
    const checkees = [
      {props: {oneOfProp: 'asdf'}, mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp]'},
      {props: {oneOfProp: 'berp'}, mess: null},
      {props: {oneOfProp: derp}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {oneOfProp: undefined}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {oneOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('oneOf.custom', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).custom(customFn)});
    const mess1 = check({oneOfProp: 'berp'});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({oneOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({oneOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
  });

  it('oneOf.isRequired.custom', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequired.custom(customFn)});
    const mess1 = check({oneOfProp: 'berp'});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({oneOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({oneOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
  });

  it('oneOf.isRequiredOrNull.custom', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequiredOrNull.custom(customFn)});
    const mess1 = check({oneOfProp: 'berp'});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({oneOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({oneOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
  });

  it('oneOf.custom.isRequired', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).custom(customFn).isRequired});
    const mess1 = check({oneOfProp: 'berp'});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({oneOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({oneOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
  });

  it('oneOf.custom.isRequiredOrNull', () => {
    const derp = {};
    const propEnum = ['berp', derp, 'lerp'];
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).custom(customFn).isRequiredOrNull});
    const mess1 = check({oneOfProp: 'berp'});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({oneOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({oneOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
  });

});
