'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('array', () => {

  it('array', () => {
    const check = checkProps({arrayProp: PropTypes.array});
    const checkees = [
      {props: {arrayProp: 'asdf'}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
      {props: {arrayProp: {}}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
      {props: {arrayProp: Object()}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
      {props: {arrayProp: []}, mess: null},
      {props: {arrayProp: Array()}, mess: null},
      {props: {}, mess: null},
      {props: {arrayProp: undefined}, mess: null},
      {props: {arrayProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('array.isRequired', () => {
    const check = checkProps({arrayProp: PropTypes.array.isRequired});
    const checkees = [
      {props: {arrayProp: 'asdf'}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
      {props: {arrayProp: []}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {arrayProp: undefined}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {arrayProp: null}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('array.isRequiredOrNull', () => {
    const check = checkProps({arrayProp: PropTypes.array.isRequiredOrNull});
    const checkees = [
      {props: {arrayProp: 'asdf'}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
      {props: {arrayProp: []}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {arrayProp: undefined}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {arrayProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('array.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
    };
    const check = checkProps({arrayProp: PropTypes.array.custom(customFn)});
    const mess1 = check({arrayProp: []});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({arrayProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
  });

  it('array.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
    };
    const check = checkProps({arrayProp: PropTypes.array.isRequired.custom(customFn)});
    const mess1 = check({arrayProp: []});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({arrayProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
  });

  it('array.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
    };
    const check = checkProps({arrayProp: PropTypes.array.isRequiredOrNull.custom(customFn)});
    const mess1 = check({arrayProp: []});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({arrayProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
  });

  it('array.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
    };
    const check = checkProps({arrayProp: PropTypes.array.custom(customFn).isRequired});
    const mess1 = check({arrayProp: []});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({arrayProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
  });

  it('array.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
    };
    const check = checkProps({arrayProp: PropTypes.array.custom(customFn).isRequiredOrNull});
    const mess1 = check({arrayProp: []});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({arrayProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
  });

});
