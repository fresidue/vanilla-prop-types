'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('func', () => {

  it('func', () => {
    const check = checkProps({funcProp: PropTypes.func});
    const checkees = [
      {props: {funcProp: 'asdf'}, mess: 'PropTypes validation error at [funcProp]: Prop must be a function when included'},
      {props: {funcProp: () => 0}, mess: null},
      {props: {funcProp: async () => 0}, mess: null},
      {props: {}, mess: null},
      {props: {funcProp: undefined}, mess: null},
      {props: {funcProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('func.isRequired', () => {
    const check = checkProps({funcProp: PropTypes.func.isRequired});
    const checkees = [
      {props: {funcProp: 'asdf'}, mess: 'PropTypes validation error at [funcProp]: Prop must be a function when included'},
      {props: {funcProp: () => 0}, mess: null},
      {props: {funcProp: async () => 0}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {funcProp: undefined}, mess: 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {funcProp: null}, mess: 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('func.isRequiredOrNull', () => {
    const check = checkProps({funcProp: PropTypes.func.isRequiredOrNull});
    const checkees = [
      {props: {funcProp: 'asdf'}, mess: 'PropTypes validation error at [funcProp]: Prop must be a function when included'},
      {props: {funcProp: () => 0}, mess: null},
      {props: {funcProp: async () => 0}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {funcProp: undefined}, mess: 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {funcProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('func.custom', () => {
    const errFunc = () => 0;
    const customFn = ({prop, propName, props}) => {
      return (prop === errFunc ? 'custom err mess' : null);
    };
    const check = checkProps({funcProp: PropTypes.func.custom(customFn)});
    const mess1 = check({funcProp: () => true});
    assert.strictEqual(mess1, null);
    const mess2 = check({funcProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({funcProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({funcProp: errFunc});
    assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
  });

  it('func.isRequired.custom', () => {
    const errFunc = () => 0;
    const customFn = ({prop, propName, props}) => {
      return (prop === errFunc ? 'custom err mess' : null);
    };
    const check = checkProps({funcProp: PropTypes.func.isRequired.custom(customFn)});
    const mess1 = check({funcProp: () => true});
    assert.strictEqual(mess1, null);
    const mess2 = check({funcProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({funcProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({funcProp: errFunc});
    assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
  });

  it('func.isRequiredOrNull.custom', () => {
    const errFunc = () => 0;
    const customFn = ({prop, propName, props}) => {
      return (prop === errFunc ? 'custom err mess' : null);
    };
    const check = checkProps({funcProp: PropTypes.func.isRequiredOrNull.custom(customFn)});
    const mess1 = check({funcProp: () => true});
    assert.strictEqual(mess1, null);
    const mess2 = check({funcProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({funcProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({funcProp: errFunc});
    assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
  });

  it('func.custom.isRequired', () => {
    const errFunc = () => 0;
    const customFn = ({prop, propName, props}) => {
      return (prop === errFunc ? 'custom err mess' : null);
    };
    const check = checkProps({funcProp: PropTypes.func.custom(customFn).isRequired});
    const mess1 = check({funcProp: () => true});
    assert.strictEqual(mess1, null);
    const mess2 = check({funcProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({funcProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({funcProp: errFunc});
    assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
  });

  it('func.custom.isRequiredOrNull', () => {
    const errFunc = () => 0;
    const customFn = ({prop, propName, props}) => {
      return (prop === errFunc ? 'custom err mess' : null);
    };
    const check = checkProps({funcProp: PropTypes.func.custom(customFn).isRequiredOrNull});
    const mess1 = check({funcProp: () => true});
    assert.strictEqual(mess1, null);
    const mess2 = check({funcProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({funcProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({funcProp: errFunc});
    assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
  });

});
