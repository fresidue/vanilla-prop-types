'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('objectOf (propType)', () => {

  it('some valid propTypes', () => {
    const valids = [
      PropTypes.any,
      PropTypes.string,
      PropTypes.number.isRequired,
      PropTypes.instanceOf(Date),
      PropTypes.objectOf(PropTypes.any.isRequired),
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({objectOfProp: PropTypes.objectOf(valid)}));
    });
  });

  it('some invalid propTypes', () => {
    const derp = {};
    const invalids = [
      {propType: {}, mess: /invalid propType function for {propName: propType}/},
      {propType: () => 0, mess: /invalid propType function for {propName: propType}/},
    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({objectOfProp: PropTypes.objectOf(invalid.propType)}), invalid.mess);
    });
  });

  it('objectOf (PropTypes.string) - default', () => {
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string)});
    const checkees = [
      {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
      {props: {objectOfProp: () => {}}, mess: null},
      {props: {objectOfProp: new Date()}, mess: null},
      {props: {objectOfProp: []}, mess: null},
      {props: {objectOfProp: [null]}, mess: null},
      {props: {objectOfProp: [undefined]}, mess: null},
      {props: {objectOfProp: [,]}, mess: null},
      {props: {objectOfProp: ['', 'asdf', '23', undefined, '']}, mess: null},
      {props: {objectOfProp: {}}, mess: null},
      {props: {objectOfProp: {a: null}}, mess: null},
      {props: {objectOfProp: {a: undefined}}, mess: null},
      {props: {}, mess: null},
      {props: {objectOfProp: undefined}, mess: null},
      {props: {objectOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('objectOf (PropTypes.string.isRequired) - default', () => {
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string.isRequired)});
    const checkees = [
      {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
      {props: {objectOfProp: () => {}}, mess: null},
      {props: {objectOfProp: new Date()}, mess: null},
      {props: {objectOfProp: []}, mess: null},
      {props: {objectOfProp: [null]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectOfProp: [undefined]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectOfProp: [,]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [objectOfProp, 3]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectOfProp: {}}, mess: null},
      {props: {objectOfProp: {a: null}}, mess: 'PropTypes validation error at [objectOfProp, a]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectOfProp: {a: undefined}}, mess: 'PropTypes validation error at [objectOfProp, a]: Prop isRequired (cannot be null or undefined)'},
      {props: {}, mess: null},
      {props: {objectOfProp: undefined}, mess: null},
      {props: {objectOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('objectOf (PropTypes.string.isRequiredOrNull) - default', () => {
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string.isRequiredOrNull)});
    const checkees = [
      {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
      {props: {objectOfProp: () => {}}, mess: null},
      {props: {objectOfProp: new Date()}, mess: null},
      {props: {objectOfProp: []}, mess: null},
      {props: {objectOfProp: [null]}, mess: null},
      {props: {objectOfProp: [undefined]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {objectOfProp: [,]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {objectOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [objectOfProp, 3]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {objectOfProp: {}}, mess: null},
      {props: {objectOfProp: {a: null}}, mess: null},
      {props: {objectOfProp: {a: undefined}}, mess: 'PropTypes validation error at [objectOfProp, a]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {}, mess: null},
      {props: {objectOfProp: undefined}, mess: null},
      {props: {objectOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('objectOf.isRequired (any) - default', () => {
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.any).isRequired});
    const checkees = [
      {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
      {props: {objectOfProp: []}, mess: null},
      {props: {objectOfProp: {}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectOfProp: undefined}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectOfProp: null}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('objectOf.isRequiredOrNull (any) - default', () => {
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.any).isRequiredOrNull});
    const checkees = [
      {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
      {props: {objectOfProp: []}, mess: null},
      {props: {objectOfProp: {}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {objectOfProp: undefined}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {objectOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('objectOf.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).custom(customFn)});
    const mess1 = check({objectOfProp: {derp: 'xxx'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectOfProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({objectOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({objectOfProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
  });

  it('objectOf.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).isRequired.custom(customFn)});
    const mess1 = check({objectOfProp: {derp: 'xxx'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({objectOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({objectOfProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
  });

  it('objectOf.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).isRequiredOrNull.custom(customFn)});
    const mess1 = check({objectOfProp: {derp: 'xxx'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({objectOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({objectOfProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
  });

  it('objectOf.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).custom(customFn).isRequired});
    const mess1 = check({objectOfProp: {derp: 'xxx'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({objectOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({objectOfProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
  });

  it('objectOf.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).custom(customFn).isRequiredOrNull});
    const mess1 = check({objectOfProp: {derp: 'xxx'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({objectOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({objectOfProp: {derp: 'derp'}});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
  });

});
