'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('instanceOf (class)', () => {

  it('some valid propClasses', () => {
    const Derp = function () {
      this.derp = 'derp';
    };
    const valids = [
      Date,
      Derp,
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({instanceOfProp: PropTypes.instanceOf(valid)}));
    });
  });

  it('some invalid propClasses', () => {
    const derp = {};
    const invalids = [
      {propClass: {}, mess: /The Class must be a callable/},
      {propClass: () => 0, mess: /The Class must be a callable/},
    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({instanceOfProp: PropTypes.instanceOf(invalid.propClass)}), invalid.mess);
    });
  });

  it('instanceOf - default', () => {
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date)});
    const checkees = [
      {props: {instanceOfProp: 'asdf'}, mess: 'PropTypes validation error at [instanceOfProp]: Prop must be an instance of class: function Date() { [native code] }'},
      {props: {instanceOfProp: new Date()}, mess: null},
      {props: {}, mess: null},
      {props: {instanceOfProp: undefined}, mess: null},
      {props: {instanceOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('instanceOf.isRequired - default', () => {
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequired});
    const checkees = [
      {props: {instanceOfProp: 'asdf'}, mess: 'PropTypes validation error at [instanceOfProp]: Prop must be an instance of class: function Date() { [native code] }'},
      {props: {instanceOfProp: new Date()}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {instanceOfProp: undefined}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {instanceOfProp: null}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('instanceOf.isRequiredOrNull - default', () => {
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequiredOrNull});
    const checkees = [
      {props: {instanceOfProp: 'asdf'}, mess: 'PropTypes validation error at [instanceOfProp]: Prop must be an instance of class: function Date() { [native code] }'},
      {props: {instanceOfProp: new Date()}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {instanceOfProp: undefined}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {instanceOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('instanceOf.custom', () => {
    const derp = new Date();
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).custom(customFn)});
    const mess1 = check({instanceOfProp: new Date()});
    assert.strictEqual(mess1, null);
    const mess2 = check({instanceOfProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({instanceOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({instanceOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
  });

  it('instanceOf.isRequired.custom', () => {
    const derp = new Date();
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequired.custom(customFn)});
    const mess1 = check({instanceOfProp: new Date()});
    assert.strictEqual(mess1, null);
    const mess2 = check({instanceOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({instanceOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({instanceOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
  });

  it('instanceOf.isRequiredOrNull.custom', () => {
    const derp = new Date();
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequiredOrNull.custom(customFn)});
    const mess1 = check({instanceOfProp: new Date()});
    assert.strictEqual(mess1, null);
    const mess2 = check({instanceOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({instanceOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({instanceOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
  });

  it('instanceOf.custom.isRequired', () => {
    const derp = new Date();
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).custom(customFn).isRequired});
    const mess1 = check({instanceOfProp: new Date()});
    assert.strictEqual(mess1, null);
    const mess2 = check({instanceOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({instanceOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({instanceOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
  });

  it('instanceOf.custom.isRequiredOrNull', () => {
    const derp = new Date();
    const customFn = ({prop, propName, props}) => {
      return (prop === derp ? 'custom err mess' : null);
    };
    const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).custom(customFn).isRequiredOrNull});
    const mess1 = check({instanceOfProp: new Date()});
    assert.strictEqual(mess1, null);
    const mess2 = check({instanceOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({instanceOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({instanceOfProp: derp});
    assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
  });

});
