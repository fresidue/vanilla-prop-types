'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('validator (propType)', () => {

  const shape = {
    derp: PropTypes.string.isRequired,
  };
  const validator = validateProps(shape);

  it('some valid validators', () => {
    const valids = [
      validateProps({}),
      validateProps({
        derp: PropTypes.string.isRequired,
      }),
      validateProps({
        derp: PropTypes.validator(validateProps({
          nextlevel: PropTypes.string,
        })),
      }),
      checkProps({}),
      checkProps({
        derp: PropTypes.string.isRequired,
      }),
      checkProps({
        derp: PropTypes.validator(validateProps({
          nextlevel: PropTypes.string,
        })),
      }),
      areValidProps({}),
      areValidProps({
        derp: PropTypes.string.isRequired,
      }),
      areValidProps({
        derp: PropTypes.validator(validateProps({
          nextlevel: PropTypes.string,
        })),
      }),
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({validatorProp: PropTypes.validator(valid)}));
    });
  });

  it('some invalid propTypes', () => {
    const derp = {};
    const invalids = [
      {
        validator: null,
        err: /the validate function must be a function/,
      },
      {
        validator: () => {},
        err: /the validate function must be \(at least nominally\) a PropTypes validator, checker, or boolean tester function/,
      },
      {
        validator: (() => {
          const validator = () => {};
          validator.isPropTypesChecker = true;
          validator.internalCheck = 'NOT A FUNCTION';
          return validator;
        })(),
        err: /the validator function must include the internalCheck function/,
      },

    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({validatorProp: PropTypes.validator(invalid.validator)}), invalid.err);
    });
  });

  it('validator - default', () => {
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      })),
    });
    const checkees = [
      {props: {validatorProp: null}, mess: null},
      {props: {validatorProp: undefined}, mess: null},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator - default (isNilable - does not do anything)', () => {
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })),
    });
    const checkees = [
      {props: {validatorProp: null}, mess: null},
      {props: {validatorProp: undefined}, mess: null},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator - isRequired (default})', () => {
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      })).isRequired,
    });
    const checkees = [
      {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator - isRequired (isNilable)', () => {
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).isRequired,
    });
    const checkees = [
      {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator - isRequired (with {isNilable: true})', () => {
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).isRequired,
    });
    const checkees = [
      {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator - isRequiredOrNull (with {isNilable: true})', () => {
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).isRequiredOrNull,
    });
    const checkees = [
      {props: {validatorProp: null}, mess: null},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator.custom (with {isNilable: true})', () => {
    const customFn = ({prop, propName, props}) => {
      if (_.get(prop, 'derp') === 'smerp') {
        return 'custom string derpy smerp error';
      }
      if (_.get(prop, 'derp') === 'knerp') {
        return new Error('custom returned derpy knerp error');
      }
      if (_.get(prop, 'derp') === 'plerp') {
        throw new Error('custom thrown derpy plerp error');
      }
      return null;
    };
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).custom(customFn),
    });
    const checkees = [
      {props: {validatorProp: null}, mess: null},
      {props: {validatorProp: undefined}, mess: null},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
      {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
      {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
      {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator.isRequired.custom (with {isNilable: true})', () => {
    const customFn = ({prop, propName, props}) => {
      if (_.get(prop, 'derp') === 'smerp') {
        return 'custom string derpy smerp error';
      }
      if (_.get(prop, 'derp') === 'knerp') {
        return new Error('custom returned derpy knerp error');
      }
      if (_.get(prop, 'derp') === 'plerp') {
        throw new Error('custom thrown derpy plerp error');
      }
      return null;
    };
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).isRequired.custom(customFn),
    });
    const checkees = [
      {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
      {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
      {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
      {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator.isRequiredOrNull.custom (with {isNilable: true})', () => {
    const customFn = ({prop, propName, props}) => {
      if (_.get(prop, 'derp') === 'smerp') {
        return 'custom string derpy smerp error';
      }
      if (_.get(prop, 'derp') === 'knerp') {
        return new Error('custom returned derpy knerp error');
      }
      if (_.get(prop, 'derp') === 'plerp') {
        throw new Error('custom thrown derpy plerp error');
      }
      return null;
    };
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).isRequiredOrNull.custom(customFn),
    });
    const checkees = [
      {props: {validatorProp: null}, mess: null},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
      {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
      {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
      {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator.custom.isRequired (with {isNilable: true})', () => {
    const customFn = ({prop, propName, props}) => {
      if (_.get(prop, 'derp') === 'smerp') {
        return 'custom string derpy smerp error';
      }
      if (_.get(prop, 'derp') === 'knerp') {
        return new Error('custom returned derpy knerp error');
      }
      if (_.get(prop, 'derp') === 'plerp') {
        throw new Error('custom thrown derpy plerp error');
      }
      return null;
    };
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).custom(customFn).isRequired,
    });
    const checkees = [
      {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
      {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
      {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
      {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator.custom.isRequiredOrNull (with {isNilable: true})', () => {
    const customFn = ({prop, propName, props}) => {
      if (_.get(prop, 'derp') === 'smerp') {
        return 'custom string derpy smerp error';
      }
      if (_.get(prop, 'derp') === 'knerp') {
        return new Error('custom returned derpy knerp error');
      }
      if (_.get(prop, 'derp') === 'plerp') {
        throw new Error('custom thrown derpy plerp error');
      }
      return null;
    };
    const check = checkProps({
      validatorProp: PropTypes.validator(validateProps({
        derp: PropTypes.string,
      }, {
        isNilable: true,
      })).custom(customFn).isRequiredOrNull,
    });
    const checkees = [
      {props: {validatorProp: null}, mess: null},
      {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {validatorProp: {derp: null}}, mess: null},
      {props: {validatorProp: {derp: undefined}}, mess: null},
      {props: {validatorProp: {derp: 'derp'}}, mess: null},
      {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
      {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
      {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('validator with multiple nested occurrences', () => {

    const check0 = areValidProps({
      barp: PropTypes.string,
      carp: PropTypes.string.error(new Error('carp error')),
    }, {isNilable: true});

    const check1 = checkProps({
      jarp: PropTypes.string,
      karp: PropTypes.shape({
        larp: PropTypes.validator(check0),
      }),
    });
    const validate = validateProps({
      parp: PropTypes.validator(check1),
    });

    const invalids = [
      {
        obj: {parp: {jarp: 23}},
        mess: /PropTypes validation error at \[parp, jarp]: Prop must be a string when included/,
      },
      {
        obj: {parp: {karp: {larp: {barp: 23}}}},
        mess: /PropTypes validation error at \[parp, karp, larp, barp]: Prop must be a string when included/,
      },
      {
        obj: {parp: {karp: {larp: {carp: 23}}}},
        mess: /carp error/,
      },
    ];
    _.each(invalids, invalid => {
      assert.throws(() => validate(invalid.obj), invalid.mess);
    });
  });

});
