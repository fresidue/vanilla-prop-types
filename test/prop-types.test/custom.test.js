'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('custom', () => {

  it('some valid and PASSING custom validation functions (i.e. allowed return values)', () => {
    const valids = [
      () => true,
      () => null,
      () => undefined,
      () => {},
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({custom: PropTypes.custom(valid)}));
      const check = checkProps({custom: PropTypes.custom(valid)});
      const messA = check({});
      assert.strictEqual(messA, null);
      const messB = check(null);
      assert.strictEqual(messB, 'PropTypes validation error: A null object is not allowed (see options to allow)');
      const messC = check(undefined);
      assert.strictEqual(messC, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
      // with isNilable
      const check2 = checkProps({custom: PropTypes.custom(valid)}, {isNilable: true});
      const messD = check2(null);
      assert.strictEqual(messD, null);
      const messE = check2(undefined);
      assert.strictEqual(messE, null);
    });
  });

  it('some valid and FAILING custom validation functions (i.e. allowed return values)', () => {
    const valids = [
      {
        func: () => false,
        mess: 'PropTypes validation error at [custom]: Prop fails custom validation function',
      },
      {
        func: () => 'custom string message',
        mess: 'PropTypes validation error at [custom]: custom string message',
      },
      {
        func: () => {
          const err = new Error('custom error. period.');
          return err; // RETURNS error
        },
        mess: 'custom error. period.',
      },
      {
        func: () => {
          const err = new Error('custom error. period.');
          throw err; // THROWS error (no difference)
        },
        mess: 'custom error. period.',
      },
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({custom: PropTypes.custom(valid.func)}));
      const check = checkProps({custom: PropTypes.custom(valid.func)});
      const mess = check({});
      assert.strictEqual(mess, valid.mess);
    });
  });

  it('some invalid custom validation functions (i.e. NON-allowed return values)', () => {
    const invalids = [
      () => 0,
      () => 23,
      () => ({}),
      () => [],
    ];
    _.each(invalids, invalid => {
      assert.doesNotThrow(() => checkProps({custom: PropTypes.custom(invalid)}));
      const check = checkProps({custom: PropTypes.custom(invalid)});
      const mess = check({});
      assert.strictEqual(mess, `PropTypes validation error at [custom]: Prop custom validation function returns ${invalid()} which is forbidden - for validation success return one of {true, null, undefined} and for validation failure return one of {false, <string>, <error>}`)
    });
  });

  it('some invalid custom propType', () => {
    const invalids = [
      0, // anything not a function basically
      null,
      undefined,
      [],
      {},
    ];
    const mess = /the custom function must be a function/;
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({custom: PropTypes.custom(invalid)}), mess);
    });
  });

  it('custom fn has access to {prop, propName, props}', () => {
    const leProps = {custom: 'derp'};
    const customFn = ({prop, propName, props}) => {
      assert.strictEqual(propName, 'custom');
      assert.strictEqual(prop, 'derp');
      assert.strictEqual(props, leProps)
    };
    const check = checkProps({custom: PropTypes.custom(customFn)});
    assert.doesNotThrow(() => check(leProps));
  });

  it('returning a string lets you craft custom validation messages (that still aggregate)', () => {
    const leProps = {custom: 'derp'};
    const customFn = ({prop, propName, props}) => {
      return 'custom error message';
    };
    const check = checkProps({custom: PropTypes.custom(customFn)});
    const message = check(leProps);
    assert.strictEqual(message, 'PropTypes validation error at [custom]: custom error message');
  });

  it('throwing an error => error.message becomes custom validation message', () => {
    const leProps = {custom: 'derp'};
    const customFn = ({prop, propName, props}) => {
      throw new Error('alternative way to set error message');
    };
    const check = checkProps({custom: PropTypes.custom(customFn)});
    const message = check(leProps);
    assert.strictEqual(message, 'alternative way to set error message');
  });

  it('returning a string and it gets treated as any other message: (gets a prefix and gets aggregated)', () => {
    const leProps = {custom: 'derp'};
    const customFn = ({prop, propName, props}) => 'anything can go wrong here';
    const check = checkProps({custom: PropTypes.custom(customFn)});
    const message = check(leProps);
    assert.strictEqual(message, 'PropTypes validation error at [custom]: anything can go wrong here');
  });

  it('returning an error and it gets preserved entirely (even up the hierarchy)', () => {
    const leProps = {custom: 'derp'};
    const customFn = ({prop, propName, props}) => {
      return new Error('something did go wrong');
    };
    const check = checkProps({custom: PropTypes.custom(customFn)});
    const message = check(leProps);
    assert.strictEqual(message, 'something did go wrong');
  });

  it('throwing an error is treated EXACTLY the same as just returning it', () => {
    const leProps = {custom: 'derp'};
    const customFn = ({prop, propName, props}) => {
      throw new Error('something did go wrong');
    };
    const check = checkProps({custom: PropTypes.custom(customFn)});
    const message = check(leProps);
    assert.strictEqual(message, 'something did go wrong');
  });

});
