'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('plainObject', () => {

  it('plainObject', () => {
    const check = checkProps({plainObjectProp: PropTypes.plainObject});
    const checkees = [
      {props: {plainObjectProp: 'asdf'}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
      {props: {plainObjectProp: {}}, mess: null},
      {props: {plainObjectProp: Object()}, mess: null},
      {props: {plainObjectProp: []}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
      {props: {plainObjectProp: () => 0}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
      {props: {plainObjectProp: async () => 0}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
      {props: {}, mess: null},
      {props: {plainObjectProp: undefined}, mess: null},
      {props: {plainObjectProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('plainObject.isRequired', () => {
    const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequired});
    const checkees = [
      {props: {plainObjectProp: 'asdf'}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
      {props: {plainObjectProp: {}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {plainObjectProp: undefined}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {plainObjectProp: null}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('plainObject.isRequiredOrNull', () => {
    const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequiredOrNull});
    const checkees = [
      {props: {plainObjectProp: 'asdf'}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
      {props: {plainObjectProp: {}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {plainObjectProp: undefined}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {plainObjectProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('plainObject.custom', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({plainObjectProp: PropTypes.plainObject.custom(customFn)});
    const mess1 = check({plainObjectProp: {some: 'plainObject'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({plainObjectProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({plainObjectProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({plainObjectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
  });

  it('plainObject.isRequired.custom', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequired.custom(customFn)});
    const mess1 = check({plainObjectProp: {some: 'plainObject'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({plainObjectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({plainObjectProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({plainObjectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
  });

  it('plainObject.isRequiredOrNull.custom', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequiredOrNull.custom(customFn)});
    const mess1 = check({plainObjectProp: {some: 'plainObject'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({plainObjectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({plainObjectProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({plainObjectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
  });

  it('plainObject.custom.isRequired', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({plainObjectProp: PropTypes.plainObject.custom(customFn).isRequired});
    const mess1 = check({plainObjectProp: {some: 'plainObject'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({plainObjectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({plainObjectProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({plainObjectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
  });

  it('plainObject.custom.isRequiredOrNull', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({plainObjectProp: PropTypes.plainObject.custom(customFn).isRequiredOrNull});
    const mess1 = check({plainObjectProp: {some: 'plainObject'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({plainObjectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({plainObjectProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({plainObjectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
  });

});
