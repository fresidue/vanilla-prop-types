'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('symbol', () => {

  it('symbol', () => {
    const check = checkProps({symbolProp: PropTypes.symbol});
    const checkees = [
      {props: {symbolProp: 'asdf'}, mess: 'PropTypes validation error at [symbolProp]: Prop must be a symbol when included'},
      {props: {symbolProp: Symbol()}, mess: null},
      {props: {symbolProp: Symbol('foo')}, mess: null},
      {props: {}, mess: null},
      {props: {symbolProp: undefined}, mess: null},
      {props: {symbolProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('symbol.isRequired', () => {
    const check = checkProps({symbolProp: PropTypes.symbol.isRequired});
    const checkees = [
      {props: {symbolProp: 'asdf'}, mess: 'PropTypes validation error at [symbolProp]: Prop must be a symbol when included'},
      {props: {symbolProp: Symbol()}, mess: null},
      {props: {symbolProp: Symbol('foo')}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {symbolProp: undefined}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {symbolProp: null}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('symbol.isRequiredOrNull', () => {
    const check = checkProps({symbolProp: PropTypes.symbol.isRequiredOrNull});
    const checkees = [
      {props: {symbolProp: 'asdf'}, mess: 'PropTypes validation error at [symbolProp]: Prop must be a symbol when included'},
      {props: {symbolProp: Symbol()}, mess: null},
      {props: {symbolProp: Symbol('foo')}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {symbolProp: undefined}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {symbolProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('symbol.custom', () => {
    const errSymbol = Symbol('err');
    const customFn = ({prop, propName, props}) => {
      return (prop === errSymbol ? 'custom err mess' : null);
    };
    const check = checkProps({symbolProp: PropTypes.symbol.custom(customFn)});
    const mess1 = check({symbolProp: Symbol()});
    assert.strictEqual(mess1, null);
    const mess2 = check({symbolProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({symbolProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({symbolProp: errSymbol});
    assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
  });

  it('symbol.isRequired.custom', () => {
    const errSymbol = Symbol('err');
    const customFn = ({prop, propName, props}) => {
      return (prop === errSymbol ? 'custom err mess' : null);
    };
    const check = checkProps({symbolProp: PropTypes.symbol.isRequired.custom(customFn)});
    const mess1 = check({symbolProp: Symbol()});
    assert.strictEqual(mess1, null);
    const mess2 = check({symbolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({symbolProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({symbolProp: errSymbol});
    assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
  });

  it('symbol.isRequiredOrNull.custom', () => {
    const errSymbol = Symbol('err');
    const customFn = ({prop, propName, props}) => {
      return (prop === errSymbol ? 'custom err mess' : null);
    };
    const check = checkProps({symbolProp: PropTypes.symbol.isRequiredOrNull.custom(customFn)});
    const mess1 = check({symbolProp: Symbol()});
    assert.strictEqual(mess1, null);
    const mess2 = check({symbolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({symbolProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({symbolProp: errSymbol});
    assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
  });

  it('symbol.custom.isRequired', () => {
    const errSymbol = Symbol('err');
    const customFn = ({prop, propName, props}) => {
      return (prop === errSymbol ? 'custom err mess' : null);
    };
    const check = checkProps({symbolProp: PropTypes.symbol.custom(customFn).isRequired});
    const mess1 = check({symbolProp: Symbol()});
    assert.strictEqual(mess1, null);
    const mess2 = check({symbolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({symbolProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({symbolProp: errSymbol});
    assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
  });

  it('symbol.custom.isRequiredOrNull', () => {
    const errSymbol = Symbol('err');
    const customFn = ({prop, propName, props}) => {
      return (prop === errSymbol ? 'custom err mess' : null);
    };
    const check = checkProps({symbolProp: PropTypes.symbol.custom(customFn).isRequiredOrNull});
    const mess1 = check({symbolProp: Symbol()});
    assert.strictEqual(mess1, null);
    const mess2 = check({symbolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({symbolProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({symbolProp: errSymbol});
    assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
  });

});
