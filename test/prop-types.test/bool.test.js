'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('bool', () => {

  it('bool', () => {
    const check = checkProps({boolProp: PropTypes.bool});
    const mess0 = check({boolProp: true});
    assert.strictEqual(mess0, null);
    const mess1 = check({boolProp: false});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, null);
    const mess3 = check({boolProp: undefined});
    assert.strictEqual(mess3, null);
    const mess4 = check({boolProp: null});
    assert.strictEqual(mess4, null);
    const mess5 = check({boolProp: 23});
    assert.strictEqual(mess5, 'PropTypes validation error at [boolProp]: Prop must be a bool when included');
  });

  it('bool.isRequired', () => {
    const check = checkProps({boolProp: PropTypes.bool.isRequired});
    const mess0 = check({boolProp: true});
    assert.strictEqual(mess0, null);
    const mess1 = check({boolProp: false});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({boolProp: undefined});
    assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({boolProp: null});
    assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
    const mess5 = check({boolProp: 23});
    assert.strictEqual(mess5, 'PropTypes validation error at [boolProp]: Prop must be a bool when included');
  });

  it('bool.isRequiredOrNull', () => {
    const check = checkProps({boolProp: PropTypes.bool.isRequiredOrNull});
    const mess0 = check({boolProp: true});
    assert.strictEqual(mess0, null);
    const mess1 = check({boolProp: false});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({boolProp: undefined});
    assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess4 = check({boolProp: null});
    assert.strictEqual(mess4, null);
    const mess5 = check({boolProp: 23});
    assert.strictEqual(mess5, 'PropTypes validation error at [boolProp]: Prop must be a bool when included');
  });

  it('bool.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === false ? 'custom err mess' : null);
    };
    const check = checkProps({boolProp: PropTypes.bool.custom(customFn)});
    const mess1 = check({boolProp: true});
    assert.strictEqual(mess1, null);
    const mess2 = check({boolProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({boolProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({boolProp: false});
    assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
  });

  it('bool.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === false ? 'custom err mess' : null);
    };
    const check = checkProps({boolProp: PropTypes.bool.isRequired.custom(customFn)});
    const mess1 = check({boolProp: true});
    assert.strictEqual(mess1, null);
    const mess2 = check({boolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({boolProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({boolProp: false});
    assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
  });

  it('bool.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === false ? 'custom err mess' : null);
    };
    const check = checkProps({boolProp: PropTypes.bool.isRequiredOrNull.custom(customFn)});
    const mess1 = check({boolProp: true});
    assert.strictEqual(mess1, null);
    const mess2 = check({boolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({boolProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({boolProp: false});
    assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
  });

  it('bool.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === false ? 'custom err mess' : null);
    };
    const check = checkProps({boolProp: PropTypes.bool.custom(customFn).isRequired});
    const mess1 = check({boolProp: true});
    assert.strictEqual(mess1, null);
    const mess2 = check({boolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({boolProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({boolProp: false});
    assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
  });

  it('bool.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === false ? 'custom err mess' : null);
    };
    const check = checkProps({boolProp: PropTypes.bool.custom(customFn).isRequiredOrNull});
    const mess1 = check({boolProp: true});
    assert.strictEqual(mess1, null);
    const mess2 = check({boolProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({boolProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({boolProp: false});
    assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
  });

});
