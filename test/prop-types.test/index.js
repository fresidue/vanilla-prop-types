'use strict';

describe('prop-types.test (type by type)', () => {

  require('./any.test');
  require('./string.test');
  require('./bool.test');
  require('./number.test');
  require('./symbol.test');
  require('./func.test');
  require('./object.test');
  require('./plain-object.test');
  require('./array.test');
  require('./one-of.test');
  require('./instance-of.test');
  require('./array-of.test');
  require('./object-of.test');
  require('./one-of-type.test');
  require('./shape.test');
  require('./validator.test');
  require('./custom.test');

});
