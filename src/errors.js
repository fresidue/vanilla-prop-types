'use strict';

const _ = require('lodash');

// ////
// error classes
// ////

class VanillaPropTypesInitializationError extends Error {
  constructor (message, type) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.type = type;
    this.isVanillaPropTypesInitializerError = true;
  }
}

// class VanillaPropTypesError extends Error {
//   constructor (message) {
//     super(message);
//     Error.captureStackTrace(this, this.constructor);
//     this.name = this.constructor.name;
//     this.isVanillaPropTypesError = true;
//   }
// }

// ////
// templates
// ////

const initializationErrorTemplates = {

  // ptoptypes
  INVALID_PROP_TYPES: 'propTypes must be an object',
  INVALID_PROP_TYPE_FUNCTION: (propName) => `invalid propType function for {propName: ${propName}}`,

  // shape options
  INSHAPE_OPTIONS_KEYS: (invalidKeys = []) => `Invalid shape options [${invalidKeys.join(', ')}]`,
  INVALID_SHAPE_OPTION_ISEXACT: () => `Invalid shape "isExact" option included. If truthy, it must be a boolean, a string \(which gets converted to an error\), an error object \(which simply gets returned\), or a function that returns or throws an error`,
  INVALID_SHAPE_OPTION_BLACKLIST: `Invalid shape option blacklist, must be an array`,
  INVALID_SHAPE_BLACKLIST_OPTION_ITEM: (item) => `Invalid shape option blacklist item, must be a string: ${item}`,
  INVALID_SHAPE_OPTION_BLACKLIST_ERROR: () => `Invalid shape option "blacklistError": must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}`,

  // error options`
  INVALID_ERROR_OPTIONS: (invalidKeys = []) => `Invalid (chained) error options [${invalidKeys.join(', ')}]`,

  // singleton options
  INVALID_SINGLETON_OPTIONS: 'For the moment, options for singleton prop is not supported',

  // main options -> will get mapped to shape options
  INVALID_MAIN_OPTIONS: (invalidKeys = [], validOptions = []) => `Invalid main options found: [${invalidKeys.join(', ')}], validOptions = [${validOptions.join(', ')}]`,
  INVALID_MAIN_OPTION_ISEXACT: () => `Invalid "isExact" option included. If truthy, it must be a boolean, a string \(which gets converted to an error\), an error object \(which simply gets returned\), or a function that returns or throws an error`,
  INVALID_MAIN_BLACKLIST: `Invalid blacklist, must be an array`,
  INVALID_MAIN_BLACKLIST_ITEM: (item) => `Invalid blacklist item, must be a string: ${item}`,
  INVALID_MAIN_BLACKLIST_ERROR: () => `Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}`,
  INVALID_MAIN_OBJECT_NULL_ERROR: () => `Invalid "objectNullError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called without any args`,
  INVALID_MAIN_OBJECT_UNDEFINED_ERROR: () => `Invalid "objectUndefinedError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called without any args`,
  INVALID_MAIN_CUSTOM_FUNCTION: () => `the global custom function must be a function if included`,
  INVALID_MAIN_OVERRIDE_ERROR: () => `Invalid "overrideError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {metaErr}`,
  INVALID_MAIN_DEFAULT_ERROR: () => `Invalid "defaultError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {metaErr}')`,

  // isRequired, isRequiredOrNull, isForbidden initializors
  INVALID_ARG_FOR_IS_REQUIRED: () => 'Initialization Error: "isRequired" checker generator must include an argument (if not nil) that is either a string or error or be a function that generates an error',
  INVALID_ARG_FOR_IS_REQUIRED_OR_NULL: () => `Initialization Error: "isRequiredOrNull" checker generator must include an argument (if not nil) that is either a string or error or be a function that generates an error'`,
  INVALID_ARG_FOR_IS_FORBIDDEN: () => `Initialization Error: "isForbidden" checker generator must include an argument (if not nil) that is either a string or error or be a function that generates an error'`,
};

const checkerErrorTemplates = {
  // fill me
};

// ////
// error-suites
// ////

const initializationErrors = _.mapValues(initializationErrorTemplates, (message, type) => (...args) => {
  const mess = _.isFunction(message) ? message(...args) : message;
  const err = new VanillaPropTypesInitializationError(mess, type);
  return err;
});

// const checkerErrors = _.mapValues(checkerErrorTemplates, (message, type) => (...args) => {
//   const mess = _.isFunction(message) ? message(...args) : message;
//   const err = new VanillaPropTypesError(mess, type);
//   // console.log({err});
//   return err;
// });

// ////
// exports
// ////

module.exports = {
  initializationErrors,
  // checkerErrors,
};
