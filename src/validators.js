'use strict';

// ////
// Z === 2
// ////

const _ = require('lodash');

const {
  SHAPE_OPTIONS_KEYS,
  CHAINED_ERROR_OPTIONS,
  MAIN_OPTIONS_KEYS,
  PROP_TYPES_IDENTIFIER_KEYS,
} = require('./constants');

const {
  initializationErrors,
} = require('./errors');

//
// PropTypes validation
//

const validatePropTypes = (propTypes) => {
  if (!_.isObject(propTypes)) {
    throw initializationErrors.INVALID_PROP_TYPES();
  }
  if (_.isFunction(propTypes) && propTypes[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesFunction]) {
    return; // good to go - the regular case
  }
  // allow plain object react-type PropType argument. this is the plain-object exception to the rule
  _.each(propTypes, (propType, propName) => {
    if (!_.isFunction(propType) || !propType[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesFunction]) {
      throw initializationErrors.INVALID_PROP_TYPE_FUNCTION(propName);
    }
  });
};

//
// checker options validation
//

const validateShapeOptions = (options) => {
  // early exit
  if (!_.size(options)) {
    return;
  }
  // eval
  const optionKeys = _.keys(options);
  const validOptionKeys = _.values(SHAPE_OPTIONS_KEYS);
  const invalidKeys = _.without(optionKeys, ...validOptionKeys);
  if (_.size(invalidKeys)) {
    throw initializationErrors.INSHAPE_OPTIONS_KEYS(invalidKeys);
  }

  // isExact
  if (options[SHAPE_OPTIONS_KEYS.isExact]) {
    const isExact = options[SHAPE_OPTIONS_KEYS.isExact];
    if (!_.isBoolean(isExact) && !_.isString(isExact) && !_.isError(isExact) && !_.isFunction(isExact)) {
      throw initializationErrors.INVALID_SHAPE_OPTION_ISEXACT();
    }
  }

  // blacklist
  if (options[SHAPE_OPTIONS_KEYS.blacklist]) {
    const blacklist = options[SHAPE_OPTIONS_KEYS.blacklist];
    if (!_.isArray(blacklist)) {
      throw initializationErrors.INVALID_SHAPE_OPTION_BLACKLIST();
    }
    _.each(blacklist, item => {
      if (!_.isString(item)) {
        throw initializationErrors.INVALID_SHAPE_BLACKLIST_OPTION_ITEM(item);
      }
    });
  }

  // blacklisterror
  if (options[SHAPE_OPTIONS_KEYS.blacklistError]) {
    const blacklistError = options[SHAPE_OPTIONS_KEYS.blacklistError];
    if (!_.isString(blacklistError) && !_.isError(blacklistError) && !_.isFunction(blacklistError)) {
      throw initializationErrors.INVALID_MAIN_BLACKLIST_ERROR();
    }
  }

};

const validateErrorOptions = (options) => {
  const optionKeys = _.keys(options);
  const validOptionKeys = _.values(CHAINED_ERROR_OPTIONS);
  const invalidKeys = _.without(optionKeys, ...validOptionKeys);
  if (_.size(invalidKeys)) {
    throw initializationErrors.INVALID_ERROR_OPTIONS(invalidKeys);
  }
};


//
// main singleton options
//

const validateSingletonOptions = (options) => {
  // not sure if any are desired
  if (!_.isNil(options)) {
    throw initializationErrors.INVALID_SINGLETON_OPTIONS();
  }
};

//
// MAIN options validation (non-singleton)
//

const validateMainOptions = (options) => {

  // gather keys (try early return)
  const optionKeys = _.keys(options);
  if (!_.size(optionKeys)) {
    return; // falsey optionKeys are all ok..
  }

  // check for extras
  const validOptionKeys = _.sortBy(_.values(MAIN_OPTIONS_KEYS));
  const invalidKeys = _.without(optionKeys, ...validOptionKeys);
  if (_.size(invalidKeys)) {
    throw initializationErrors.INVALID_MAIN_OPTIONS(invalidKeys, validOptionKeys);
  }

  // isExact
  if (options[MAIN_OPTIONS_KEYS.isExact]) {
    const isExact = options[MAIN_OPTIONS_KEYS.isExact];
    if (!_.isBoolean(isExact) && !_.isString(isExact) && !_.isError(isExact) && !_.isFunction(isExact)) {
      throw initializationErrors.INVALID_MAIN_OPTION_ISEXACT();
    }
  }

  // blacklist
  if (options[MAIN_OPTIONS_KEYS.blacklist]) {
    const blacklist = options[MAIN_OPTIONS_KEYS.blacklist];
    if (!_.isArray(blacklist)) {
      throw initializationErrors.INVALID_MAIN_BLACKLIST();
    }
    _.each(blacklist, item => {
      if (!_.isString(item)) {
        throw initializationErrors.INVALID_MAIN_BLACKLIST_ITEM(item);
      }
    });
  }

  // blacklisterror
  if (options[MAIN_OPTIONS_KEYS.blacklistError]) {
    const blacklistError = options[MAIN_OPTIONS_KEYS.blacklistError];
    if (!_.isString(blacklistError) && !_.isError(blacklistError) && !_.isFunction(blacklistError)) {
      throw initializationErrors.INVALID_MAIN_BLACKLIST_ERROR();
    }
  }

  // objectNullError
  if (options[MAIN_OPTIONS_KEYS.objectNullError]) {
    let objectNullError = options[MAIN_OPTIONS_KEYS.objectNullError];
    if (_.isFunction(objectNullError)) {
      try {
        objectNullError = objectNullError();
      } catch (err) {
        objectNullError = err;
      }
    }
    if (!_.isString(objectNullError) && !_.isError(objectNullError)) {
      throw initializationErrors.INVALID_MAIN_OBJECT_NULL_ERROR();
    }
  }

  // objectUndefinedError
  if (options[MAIN_OPTIONS_KEYS.objectUndefinedError]) {
    let objectUndefinedError = options[MAIN_OPTIONS_KEYS.objectUndefinedError];
    if (_.isFunction(objectUndefinedError)) {
      try {
        objectUndefinedError = objectUndefinedError();
      } catch (err) {
        objectUndefinedError = err;
      }
    }
    if (!_.isString(objectUndefinedError) && !_.isError(objectUndefinedError)) {
      throw initializationErrors.INVALID_MAIN_OBJECT_UNDEFINED_ERROR();
    }
  }

  if (options[MAIN_OPTIONS_KEYS.custom]) {
    if (!_.isFunction(options[MAIN_OPTIONS_KEYS.custom])) {
      throw initializationErrors.INVALID_MAIN_CUSTOM_FUNCTION();
    }
  }
  if (options[MAIN_OPTIONS_KEYS.overrideError]) {
    const overrideError = options[MAIN_OPTIONS_KEYS.overrideError];
    if (!_.isString(overrideError) && !_.isError(overrideError) && !_.isFunction(overrideError)) {
      throw initializationErrors.INVALID_MAIN_OVERRIDE_ERROR();
    }
  }
  if (options[MAIN_OPTIONS_KEYS.defaultError]) {
    const defaultError = options[MAIN_OPTIONS_KEYS.defaultError];
    if (!_.isString(defaultError) && !_.isError(defaultError) && !_.isFunction(defaultError)) {
      throw initializationErrors.INVALID_MAIN_DEFAULT_ERROR();
    }
  }
};

module.exports = {
  validatePropTypes,
  validateShapeOptions,
  validateErrorOptions,
  validateSingletonOptions,
  validateMainOptions,
};
