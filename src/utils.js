'use strict';

// ////
// Z === 1
// ////

const _ = require('lodash');

const compactAddress = address => {
  const compactedAddress = _.filter(address, item => !_.isNil(item));
  return compactedAddress;
};

const extractExtraPropNames = (props, propTypes) => {
  const propNames = _.keys(props);
  const allowedPropNames = _.keys(propTypes);
  const extraPropNames = _.without(propNames, ...allowedPropNames);
  return extraPropNames;
};

module.exports = {
  compactAddress,
  extractExtraPropNames,
};
