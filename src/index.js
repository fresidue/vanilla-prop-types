'use strict';

const _ = require('lodash');

const {
  assembleMetaMessage,
} = require('./meta-err');

const {
  PROP_TYPES_IDENTIFIER_KEYS,
} = require('./constants')

const {
  PropTypes,
} = require('./prop-types');

// ////
// the primary workhorse
// ////

const internalCheckProps = require('./internal-check');

// ////
// exports
// ////

const checkProps = (propTypes, options) => {
  const internalCheck = internalCheckProps(propTypes, options);
  const check = (props) => {
    const metaErr = internalCheck(props);
    const message = assembleMetaMessage(metaErr);
    return message;
  };
  check[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesChecker] = true;
  check[PROP_TYPES_IDENTIFIER_KEYS.internalCheck] = internalCheck;
  return check;
};

const validateProps = (propTypes, options) => {
  const internalCheck = internalCheckProps(propTypes, options);
  const validate = (props) => {
    const metaErr = internalCheck(props);
    if (!metaErr) {
      return;
    }
    if (_.isError(metaErr)) {
      throw metaErr;
    }
    const message = assembleMetaMessage(metaErr);
    throw new Error(message);
  };
  // and done
  validate[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesValidator] = true;
  validate[PROP_TYPES_IDENTIFIER_KEYS.internalCheck] = internalCheck;
  return validate;
};

const areValidProps = (propTypes, options) => {
  const internalCheck = internalCheckProps(propTypes, options);
  const areValid = (props) => {
    const metaErr = internalCheck(props);
    return !metaErr;
  };
  areValid[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesBooleanTester] = true;
  areValid[PROP_TYPES_IDENTIFIER_KEYS.internalCheck] = internalCheck;
  return areValid;
};


//
// Exports
//

module.exports = {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
};
