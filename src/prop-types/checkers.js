'use strict';

const _ = require('lodash');
const isCallable = require('is-callable');

const {
  MESSAGE_ITEMS_CROP,
  PROP_TYPES_IDENTIFIER_KEYS,
  SHAPE_OPTIONS_KEYS,
} = require('../constants')

const {
  validatePropTypes,
  validateShapeOptions,
} = require('../validators');

const {
  compactAddress,
  extractExtraPropNames,
} = require('../utils');

//
// simple checkers
//

const isAny = ({prop, propName, props}) => {
  return null;
};

const isString = ({prop, propName, props}) => {
  if (_.isString(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be a string when included'};
};

const isBoolean = ({prop, propName, props}) => {
  if (_.isBoolean(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be a bool when included'};
};

const isNumber = ({prop, propName, props}) => {
  if (_.isNumber(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be a number when included'};
};

const isSymbol = ({prop, propName, props}) => {
  if (_.isSymbol(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be a symbol when included'};
};

const isFunction = ({prop, propName, props}) => {
  if (_.isFunction(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be a function when included'};
};

const isObject = ({prop, propName, props}) => {
  if (_.isObject(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be an object when included'};
};

const isPlainObject = ({prop, propName, props}) => {
  if (_.isPlainObject(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be a plain object when included'};
};

const isArray = ({prop, propName, props}) => {
  if (_.isArray(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: compactAddress([propName]), description: 'Prop must be an array when included'};
};

//
// memoized checkers - simple
//

const validateEnum = propEnum => {
  if (!_.isArray(propEnum)) {
    throw new Error('propEnum must be an array');
  }
  if (propEnum.length !== _.uniq(propEnum).length) {
    throw new Error('propEnum must contain unique values');
  }
  const compactedEnum = _.filter(propEnum, val => !_.isNil(val));
  if (propEnum.length !== compactedEnum.length) {
    throw new Error('propEnum cannot contain undefined or null');
  }
};
const isOneOf = propEnum => {
  validateEnum(propEnum);
  const checker = ({prop, propName, props}) => {
    if (_.includes(propEnum, prop) || _.isNil(prop)) {
      return null;
    }
    const isCropped = propEnum.length > MESSAGE_ITEMS_CROP;
    const listItems = !isCropped ? propEnum : propEnum.slice(0, MESSAGE_ITEMS_CROP);
    return {
      address: compactAddress([propName]),
      description: `Prop must be contained in enum: [${listItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
    };
  };
  return checker;
};

const validateClass = propClass => {
  if (!isCallable(propClass) || !propClass.prototype) {
    throw new Error('The Class must be a callable');
  }
};
const isInstanceOf = propClass => {
  validateClass(propClass);
  const checker = ({prop, propName, props}) => {
    if (prop instanceof propClass || _.isNil(prop)) {
      return null;
    }
    return {
      address: compactAddress([propName]),
      description: `Prop must be an instance of class: ${propClass}`,
    };
  };
  return checker;
};

const isArrayOf = propType => {
  validatePropTypes({propType});
  const checker =  ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return null;
    }
    if (!_.isArray(prop)) {
      return {
        address: compactAddress([propName]),
        description: 'Prop must be an array (PropTypes.arrayOf requirement)',
      };
    }
    let invalidItemRes = null;
    _.each(prop, (item, index) => {
      invalidItemRes = propType({prop: item, propName: index, props: prop});
      if (invalidItemRes) {
        return false; // early exit
      }
    });
    if (!invalidItemRes) {
      return null;
    }
    // pass errors upwards directly
    if (_.isError(invalidItemRes)) {
      return invalidItemRes;
    }
    // aggregate description & address info
    return {
      address: compactAddress([propName, ...invalidItemRes.address]),
      description: invalidItemRes.description,
    };
  };
  return checker;
};

const isObjectOf = propType => {
  validatePropTypes({propType});
  const checker =  ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return null;
    }
    if (!_.isObject(prop)) {
      return {
        address: compactAddress([propName]),
        description: 'Prop must be an object (PropTypes.objectOf requirement)',
      };
    }
    let invalidItemRes = null;
    _.each(prop, (item, itemName) => {
      invalidItemRes = propType({prop: item, propName: itemName, props: prop});
      if (invalidItemRes) {
        return false; // early exit
      }
    });
    if (!invalidItemRes) {
      return null;
    }
    // pass errors upwards directly
    if (_.isError(invalidItemRes)) {
      return invalidItemRes;
    }
    // aggregate description & address info
    return {
      address: compactAddress([propName, ...invalidItemRes.address]),
      description: invalidItemRes.description,
    };
  };
  return checker;
};

const isOneOfType = propTypesArray => {
  if (!_.isArray(propTypesArray)) {
    throw new Error(`the "oneOfType" input must be an array of PropTypes`);
  }
  if (!_.size(propTypesArray)) {
    throw new Error(`the "oneOfType" input array must have at least one PropTypes item`);
  }
  validatePropTypes(propTypesArray);
  const checker = ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return null;
    }
    let foundValidType = false;
    _.each(propTypesArray, propType => {
      const metaErr = propType({prop, propName, props});
      if (!metaErr) {
        foundValidType = true;
        return false; // early exit
      }
    });
    if (foundValidType) {
      return null;
    }
    return {
      address: compactAddress([propName]),
      description: `Prop must be one of ${propTypesArray.length} given PropTypes (PropTypes.oneOfType requirement)`,
    };
  };
  return checker;
};

//
// shape (is special)
//


const isShape = (propsShape, options = {}) => {
  if (!_.isObject(propsShape)) {
    throw new Error(`the "shape" input must be an object with PropTypes as props`);
  }
  validatePropTypes(propsShape);
  validateShapeOptions(options);
  const checker = ({prop, propName, props}) => {

    if (_.isNil(prop)) {
      return null;
    }
    if (!_.isPlainObject(prop)) {
      return {
        address: compactAddress([propName]),
        description: `The shape container must be a plain object (both "props" and "shape" are really synonymous to plainObject)`,
      };
    }

    // isExact check
    const extraPropNames = extractExtraPropNames(prop, propsShape);
    if (options[SHAPE_OPTIONS_KEYS.isExact] && extraPropNames.length) {
      // simple extracts
      const isCropped = extraPropNames.length > MESSAGE_ITEMS_CROP;
      const croppedItems = !isCropped ? extraPropNames : extraPropNames.slice(0, MESSAGE_ITEMS_CROP);
      // extract isExact value. if function, extract and replace
      let isExact = options[SHAPE_OPTIONS_KEYS.isExact];
      if (_.isFunction(isExact)) {
        try {
          isExact = isExact({prop, propName, props, extraPropNames});
        } catch (err) {
          isExact = err;
        }
      }
      // evaluate (possibly resolved) isExact
      if (_.isBoolean(isExact)) {
        return {
          address: compactAddress([propName]),
          description: `PropTypes.shape with isExact=true has extra (unallowed) props: [${croppedItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
        };
      }
      if (_.isString(isExact)) {
        return {
          address: compactAddress([propName]),
          description: isExact,
        };
      }
      if (_.isError(isExact)) {
        return isExact;
      }
      // default is invalid
      return {
        address: compactAddress([propName]),
        description: `Invalid "isExact" option, which when truthy must be a boolean, string, an error object, or a function that throws an error, or returns a boolean, string, or an error. The error generator gets called with {prop, propnName, props, extraPropNames}). Extraneous props found with isExact=errorGenerator: [${croppedItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
      };
    }

    // blacklist check
    if (options[SHAPE_OPTIONS_KEYS.blacklist]) {
      const blacklist = options[SHAPE_OPTIONS_KEYS.blacklist];
      const subPropNames = _.keys(prop);
      const blacklistedPropsFound = _.intersection(subPropNames, blacklist);
      if (_.size(blacklistedPropsFound)) {
        // extract
        let blacklistError = options[SHAPE_OPTIONS_KEYS.blacklistError];
        const isCropped = blacklistedPropsFound.length > MESSAGE_ITEMS_CROP;
        const croppedItems = !isCropped ? blacklistedPropsFound : blacklistedPropsFound.slice(0, MESSAGE_ITEMS_CROP);
        // blacklist error is strictly not necessary
        if (!blacklistError) {
          return {
            address: compactAddress([propName]),
            description: `Props from blacklist found, with keys: [${croppedItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
          };
        }
        // resolve functionals
        if (_.isFunction(blacklistError)) {
          try {
            blacklistError = blacklistError({prop, propName, props, blacklistedPropsFound})
          } catch (err) {
            blacklistError = err;
          }
        }
        // simple blacklistError case
        if (_.isString(blacklistError)) {
          return {
            address: compactAddress([propName]),
            description: blacklistError,
          };
        }
        if (_.isError(blacklistError)) {
          return blacklistError;
        }
        // default is invalid
        return {
          address: compactAddress([propName]),
          description: `Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}). Props with blacklistedPropsFound found: [${croppedItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
        };
      }
    }

    // primary subProps check
    let subPropMetaErr = null;
    _.each(propsShape, (subPropType, subPropName) => {
      const subProp = _.get(prop, subPropName);
      subPropMetaErr = subPropType({prop: subProp, propName: subPropName, props: prop});
      if (subPropMetaErr) {
        return false; // early return
      }
    });
    if (!subPropMetaErr) {
      return null;
    }
    // pass errors upwards directly
    if (_.isError(subPropMetaErr)) {
      return subPropMetaErr;
    }
    // aggregate descriptions as an address
    return {
      address: compactAddress([propName, ...subPropMetaErr.address]),
      description: subPropMetaErr.description,
    };
  };
  return checker;
};

//
// validator (recurses - a type of shape)
//

const isValidator = (validate) => {
  if (!_.isFunction(validate)) {
    throw new Error(`the validate function must be a function`);
  }
  const allowedValidatorTypes = [
    PROP_TYPES_IDENTIFIER_KEYS.isPropTypesValidator,
    PROP_TYPES_IDENTIFIER_KEYS.isPropTypesChecker,
    PROP_TYPES_IDENTIFIER_KEYS.isPropTypesBooleanTester,
  ];
  if (!_.some(allowedValidatorTypes, typeKey => validate[typeKey])) {
    throw new Error(`the validate function must be (at least nominally) a PropTypes validator, checker, or boolean tester function`);
  }
  const internalCheck = validate[PROP_TYPES_IDENTIFIER_KEYS.internalCheck];
  if (!_.isFunction(internalCheck)) {
    throw new Error(`the validator function must include the internalCheck function`);
  }
  const checker = (...args) => {
    const {prop, propName, props} = args[0] || {};
    if (_.isNil(prop)) {
      return null;
    }
    let metaErr;
    try {
      metaErr = internalCheck(prop);
    } catch (err) {
      metaErr = err;
    }
    if (!metaErr) {
      return null;
    }
    // pass errors upwards directly
    if (_.isError(metaErr)) {
      return metaErr;
    }
    // otherwise just continue the aggregation
    return {
      address: compactAddress([propName, ...metaErr.address]),
      description: metaErr.description,
    };
  };
  return checker;
};

//
// custom - strangely unencumbered
//

const isCustom = customFn => {
  if (!_.isFunction(customFn)) {
    throw new Error(`the custom function must be a function`);
  }
  const checker = ({prop, propName, props}) => {
    let res = undefined;
    try {
      res = customFn({prop, propName, props});
    } catch (err) {
      res = err;
    }
    if (_.isNil(res)) {
      return null;
    }
    // handle boolean cases so that new Boolean(true/false) and true/false both work..
    if (_.isBoolean(res) && res) {
      return null;
    }
    if (_.isError(res)) {
      return res; // pass on errors upwards differently from strings
    }
    if (_.isBoolean(res) && !res) {
      return {
        address: compactAddress([propName]),
        description: 'Prop fails custom validation function',
      };
    }
    if (_.isString(res)) {
      return {
        address: compactAddress([propName]),
        description: res,
      };
    }
    return {
      address: compactAddress([propName]),
      description: `Prop custom validation function returns ${res} which is forbidden - for validation success return one of {true, null, undefined} and for validation failure return one of {false, <string>, <error>}`,
    };
  };
  return checker;
};

//
// exports - all atomic checkers
//

module.exports = {
  isAny,
  isString,
  isBoolean,
  isNumber,
  isSymbol,
  isFunction,
  isObject,
  isPlainObject,
  isArray,
  isOneOf,
  isInstanceOf,
  isArrayOf,
  isObjectOf,
  isOneOfType,
  isShape,
  isValidator,
  isCustom,
};
