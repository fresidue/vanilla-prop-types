'use strict';

const _ = require('lodash');

const {
  isAny,
  isString,
  isBoolean,
  isNumber,
  isSymbol,
  isFunction,
  isObject,
  isPlainObject,
  isArray,
  isOneOf,
  isInstanceOf,
  isArrayOf,
  isObjectOf,
  isOneOfType,
  isShape,
  isValidator,
  isCustom,
} = require('./checkers');

const {
  wrapFinal,
  wrapSkinnyFinal,
  wrapForbidden,
} = require('./chainers');


// generate the PropTypes - fully massaged
const PropTypes = {
  any: wrapFinal(isAny),
  string: wrapFinal(isString),
  bool: wrapFinal(isBoolean),
  number: wrapFinal(isNumber),
  symbol: wrapFinal(isSymbol),
  func: wrapFinal(isFunction),
  object: wrapFinal(isObject),
  plainObject: wrapFinal(isPlainObject),
  array: wrapFinal(isArray),
  oneOf: (...args) => wrapFinal(isOneOf(...args)),
  instanceOf: (...args) => wrapFinal(isInstanceOf(...args)),
  arrayOf: (...args) => wrapFinal(isArrayOf(...args)),
  objectOf: (...args) => wrapFinal(isObjectOf(...args)),
  oneOfType: (...args) => wrapFinal(isOneOfType(...args)),
  shape: (...args) => wrapFinal(isShape(...args)),
  validator: (...args) => wrapFinal(isValidator(...args)),
  custom: (...args) => wrapSkinnyFinal(isCustom(...args)),
  isForbidden: wrapForbidden(),
};

//
// Exports
//

module.exports = {
  PropTypes,
};
