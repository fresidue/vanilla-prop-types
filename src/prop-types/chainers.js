'use strict';

const _ = require('lodash');

const {
  populateMetaMessage,
} = require('../meta-err');

const {
  CHAINED_ERROR_OPTIONS,
  PROP_TYPES_IDENTIFIER_KEYS,
} = require('../constants')

const {
  validateErrorOptions,
} = require('../validators');

const {
  compactAddress,
} = require('../utils');

const {
  initializationErrors,
} = require('../errors');

const {
  isCustom,
} = require('./checkers');

//
// local utility - mutating dirtyness
//

const addPropTypesIdentifier = checker => {
  checker[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesFunction] = true;
};

//
// wrappers - provides the chaining
//


// injects custom error override
const wrapErrorGen = (propChecker) => {
  const wrappedCheckerGen = (errorGen, options = {}) => {
    validateErrorOptions(options);
    const wrappedChecker = ({prop, propName, props}) => {
      // first do the primary check
      const metaErr = propChecker({prop, propName, props});
      if (!metaErr) {
        return null;
      }
      if (_.isError(metaErr) && options[CHAINED_ERROR_OPTIONS.isWeak]) {
        return metaErr;
      }
      // extract/generate the custom error object
      if (_.isString(errorGen)) {
        const error = new Error(errorGen);
        return error;
      }
      if (_.isError(errorGen)) {
        const error = errorGen;
        return error;
      }
      if (_.isFunction(errorGen)) {
        // we do NOT want to "try" the supplied function bofore it is needed
        let errorGenRes;
        try {
          errorGenRes = errorGen({prop, propName, props, metaErr: populateMetaMessage(metaErr)});
        } catch (err) {
          errorGenRes = err;
        }
        if (_.isString(errorGenRes)) {
          const error = new Error(errorGenRes);
          return error;
        }
        if (_.isError(errorGenRes)) {
          const error = errorGenRes;
          return error;
        }
        return {
          address: compactAddress(metaErr.address),
          description: `Invalid custom error generator function (it must return a string or error or throw an error). Original error description: ${metaErr.description}`,
        };
      }
      return {
        address: compactAddress(metaErr.address),
        description: `Invalid custom error generator (it must be either a string, an error, or a function that returns a string or error or throws an error). Original error description: ${metaErr.description}`,
      };
    };
    addPropTypesIdentifier(wrappedChecker);
    return wrappedChecker;
  };
  return wrappedCheckerGen;
};



const wrapCustom = (propChecker, {extended} = {}) => {
  const wrappedCheckerGen = (customFn) => {
    const customChecker = isCustom(customFn);
    const wrappedChecker = ({prop, propName, props}) => {
      // first do the primary check
      const metaErr = propChecker({prop, propName, props});
      if (metaErr !== null) {
        return metaErr;
      }
      // then do the custom check
      const customMetaErr = customChecker({prop, propName, props});
      return customMetaErr;
    };
    addPropTypesIdentifier(wrappedChecker);
    wrappedChecker.error = wrapErrorGen(wrappedChecker);

    if (extended) {
      wrappedChecker.isRequired = wrapRequired(wrappedChecker);
      wrappedChecker.isRequiredOrNull = wrapRequiredOrNull(wrappedChecker);
      wrappedChecker.isRequired.error = wrapErrorGen(wrappedChecker.isRequired);
      wrappedChecker.isRequiredOrNull.error = wrapErrorGen(wrappedChecker.isRequiredOrNull);
    }

    return wrappedChecker;
  };
  return wrappedCheckerGen;
};


const wrapRequired = (propChecker, {extended} = {}) => {

  // "isRequired" is a special case as it needs to be able to act as a checker directly
  // as well as be able to take a single errorGen argument to create a checker..
  const wrappedCheckerGen = (checkerArg) => {

    if (!_.isString(checkerArg) && !_.isError(checkerArg) && !_.isFunction(checkerArg) && !_.isPlainObject(checkerArg) && !_.isNil(checkerArg)) {
      throw initializationErrors.INVALID_ARG_FOR_IS_REQUIRED();
    }

    // the case where "isRequired" is being used directly, i.e. {props, prop, propName}
    if (_.isPlainObject(checkerArg)) { // this could really be validated to an object with PropTypes props
      if (_.isNil(checkerArg.prop)) {
        return {
          address: compactAddress([checkerArg.propName]),
          description: 'Prop isRequired (cannot be null or undefined)',
        };
      }
      return propChecker(checkerArg);
    }

    const wrappedChecker = ({prop, propName, props}) => {
      // valid case
      if (!_.isNil(prop)) {
        return propChecker({prop, propName, props});
      }
      // otherwise invalid case - checkerArg dependent
      if (_.isNil(checkerArg)) {
        return {
          address: compactAddress([propName]),
          description: 'Prop isRequired (cannot be null or undefined)',
        };
      }
      if (_.isString(checkerArg)) {
        return {
          address: compactAddress([propName]),
          description: checkerArg,
        };
      }
      if (_.isError(checkerArg)) {
        return checkerArg;
      }
      if (_.isFunction(checkerArg)) {
        let generatedError;
        try {
          generatedError = checkerArg({prop, propName, props});
        } catch (err) {
          generatedError = err;
        }
        if (_.isString(generatedError)) {
          return {
            address: compactAddress([propName]),
            description: generatedError,
          };
        }
        if (_.isError(generatedError)) {
          return generatedError;
        }
        return {
          address: compactAddress([propName]),
          description: `Invalid isRequired error generator function (it must return a string or error or throw an error)`,
        };
      }
    };
    addPropTypesIdentifier(wrappedChecker);
    if (extended) {
      wrappedChecker.custom = wrapCustom(wrappedChecker, {extended: false});
      wrappedChecker.error = wrapErrorGen(wrappedChecker);
    }
    return wrappedChecker;
  };
  addPropTypesIdentifier(wrappedCheckerGen);
  if (extended) {
    wrappedCheckerGen.custom = wrapCustom(wrappedCheckerGen, {extended: false});
    wrappedCheckerGen.error = wrapErrorGen(wrappedCheckerGen);
  }
  return wrappedCheckerGen;
};

const wrapRequiredOrNull = (propChecker, {extended} = {}) => {

    // "wrapRequiredOrNull" is a special case as it needs to be able to act as a checker directly
    // as well as be able to take a single errorGen argument to create a checker..
    const wrappedCheckerGen = (checkerArg) => {
      if (!_.isString(checkerArg) && !_.isError(checkerArg) && !_.isFunction(checkerArg) && !_.isPlainObject(checkerArg) && !_.isNil(checkerArg)) {
        throw initializationErrors.INVALID_ARG_FOR_IS_REQUIRED_OR_NULL();
      }

      // the case where "isRequired" is being used directly
      if (_.isPlainObject(checkerArg)) {
        if (_.isUndefined(checkerArg.prop)) {
          return {
            address: compactAddress([checkerArg.propName]),
            description: 'Prop isRequiredOrNull (cannot be undefined)',
          };
        }
        return propChecker(checkerArg);
      }

      const wrappedChecker = ({prop, propName, props}) => {
        // valid case
        if (!_.isUndefined(prop)) {
          return propChecker({prop, propName, props});
        }
        // otherwise invalid case - checkerArg dependent
        if (_.isNil(checkerArg)) {
          return {
            address: compactAddress([propName]),
            description: 'Prop isRequiredOrNull (cannot be undefined)',
          };
        }
        if (_.isString(checkerArg)) {
          return {
            address: compactAddress([propName]),
            description: checkerArg,
          };
        }
        if (_.isError(checkerArg)) {
          return checkerArg;
        }
        if (_.isFunction(checkerArg)) {
          let generatedError;
          try {
            generatedError = checkerArg({prop, propName, props});
          } catch (err) {
            generatedError = err;
          }
          if (_.isString(generatedError)) {
            return {
              address: compactAddress([propName]),
              description: generatedError,
            };
          }
          if (_.isError(generatedError)) {
            return generatedError;
          }
          return {
            address: compactAddress([propName]),
            description: `Invalid isRequiredOrNull error generator function (it must return a string or error or throw an error)`,
          };
        }
      };
      addPropTypesIdentifier(wrappedChecker);
      if (extended) {
        wrappedChecker.custom = wrapCustom(wrappedChecker, {extended: false});
        wrappedChecker.error = wrapErrorGen(wrappedChecker);
      }
      return wrappedChecker;
    };
    addPropTypesIdentifier(wrappedCheckerGen);
    if (extended) {
      wrappedCheckerGen.custom = wrapCustom(wrappedCheckerGen, {extended: false});
      wrappedCheckerGen.error = wrapErrorGen(wrappedCheckerGen);
    }
    return wrappedCheckerGen;
};


const wrapForbidden = () => {
    // "wrapForbidden" is a special case as it needs to be able to act as a checker directly
    // as well as be able to take a single errorGen argument to create a checker..
    const wrappedCheckerGen = (checkerArg) => {
      if (!_.isString(checkerArg) && !_.isError(checkerArg) && !_.isFunction(checkerArg) && !_.isPlainObject(checkerArg) && !_.isNil(checkerArg)) {
        throw initializationErrors.INVALID_ARG_FOR_IS_FORBIDDEN();
      }

      // the case where "isForbidden" is being used directly
      if (_.isPlainObject(checkerArg)) {
        if (_.isNil(checkerArg.prop)) {
          return null; // the valid case
        }
        return {
          address: compactAddress([checkerArg.propName]),
          description: 'Prop isForbidden (must be either undefined or null)',
        };
      }
      const wrappedChecker = ({prop, propName, props}) => {
        // valid case
        if (_.isNil(prop)) {
          return null;
        }
        // otherwise invalid case - checkerArg dependent
        if (_.isNil(checkerArg)) {
          return {
            address: compactAddress([propName]),
            description: 'Prop isForbidden (must be either undefined or null)',
          };
        }
        if (_.isString(checkerArg)) {
          return {
            address: compactAddress([propName]),
            description: checkerArg,
          };
        }
        if (_.isError(checkerArg)) {
          return checkerArg;
        }
        if (_.isFunction(checkerArg)) {
          let generatedError;
          try {
            generatedError = checkerArg({prop, propName, props});
          } catch (err) {
            generatedError = err;
          }
          if (_.isString(generatedError)) {
            return {
              address: compactAddress([propName]),
              description: generatedError,
            };
          }
          if (_.isError(generatedError)) {
            return generatedError;
          }
          return {
            address: compactAddress([propName]),
            description: `Invalid isForbidden error generator function (it must return a string or error or throw an error)`,
          };
        }
      };
      addPropTypesIdentifier(wrappedChecker);
      return wrappedChecker;
    };
    addPropTypesIdentifier(wrappedCheckerGen);
    return wrappedCheckerGen;
};

// ////
// wrappers
// ////

const wrapFinal = (propChecker) => {
  // simple wrapper so original propChecker object does not get mutated
  const wrappedChecker = ({prop, propName, props}) => propChecker({prop, propName, props});
  // mutate
  wrappedChecker.isRequired = wrapRequired(wrappedChecker, {extended: true});
  wrappedChecker.isRequiredOrNull = wrapRequiredOrNull(wrappedChecker, {extended: true});
  wrappedChecker.custom = wrapCustom(wrappedChecker, {extended: true});
  wrappedChecker.error = wrapErrorGen(wrappedChecker);
  // done
  addPropTypesIdentifier(wrappedChecker);
  return wrappedChecker;
};

const wrapSkinnyFinal = (propChecker) => {
  // simple wrapper so original propChecker object does not get mutated
  const wrappedChecker = ({prop, propName, props}) => propChecker({prop, propName, props});
  // done
  addPropTypesIdentifier(wrappedChecker);
  return wrappedChecker;
};

// ////
// exports
// ////

module.exports = {
  wrapFinal,
  wrapSkinnyFinal,
  wrapForbidden,
};
