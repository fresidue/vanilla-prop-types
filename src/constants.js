'use strict';

const MESSAGE_ITEMS_CROP = 3;

// this needs to be rethought
const PROP_TYPES_IDENTIFIER_KEYS = {
  isPropTypesFunction: 'isPropTypesFunction',
  isPropTypesChecker: 'isPropTypesChecker',
  isPropTypesValidator: 'isPropTypesValidator',
  isPropTypesBooleanTester: 'isPropTypesBooleanTester',
  internalCheck: 'internalCheck',
};

const SHAPE_OPTIONS_KEYS = {
  isExact: 'isExact', // no props other than those defined in shape are allowed
  blacklist: 'blacklist', // a list of propNames that are NOT allowed
  blacklistError: 'blacklistError', // if custom blacklist error injection is needed
};

// propChain.error - should be replaced with defaulError (or augmented?)
const CHAINED_ERROR_OPTIONS = {
  isWeak: 'isWeak', // if isWeak then error() only triggers on error-messages (NOT Errors)
};

const MAIN_OPTIONS_KEYS = {
  // // // duplicates shape options
  isExact: 'isExact', // no props other than those with PropTypes are allowed (default false)
  blacklist: 'blacklist', // a list of propNames that are NOT allowed
  blacklistError: 'blacklistError', // if custom blacklist error injection is needed
  // // // nillable & nullable
  isNilable: 'isNilable', // the object being checked is allowed to be null or undefined (default false)
  isNullable: 'isNullable', // the object being checked is allowed to be null (default false)
  objectNullError: 'objectNullError', // custom string/error/errorGenerator for when main object is null and is not allowed to be (overrides default behaviour)
  objectUndefinedError: 'objectUndefinedError', // custom string/error/errorGenerator for when main object is undefined and is not allowed to be (overrides default behaviour)
  // // // generates shape chaining space
  custom: 'custom', // a custom validation function can be injected from the getgo (that doesn't have to be attached to a specific prop)
  defaultError: 'defaultError', // custom string/error/errorGenerator that overrides any message, but does NOT override any errors coming through
  overrideError: 'overrideError', // custom string/error/errorGenerator that override ALL other errors/messages encountered
};

module.exports = {
  MESSAGE_ITEMS_CROP,
  PROP_TYPES_IDENTIFIER_KEYS,
  SHAPE_OPTIONS_KEYS,
  CHAINED_ERROR_OPTIONS,
  MAIN_OPTIONS_KEYS,
};
